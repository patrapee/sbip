package resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AssertionCompare {

	//validate test on the page
	public static boolean ValidateText (WebDriver driver, String expectValue)
	{
		boolean result = false;
		if(driver.getPageSource().contains(expectValue))
		{
			result = true;
		}
		
		return result;
	}
	
	
	//validate attribute existing on webpage
	public static boolean ValidateAttribute (WebDriver driver, String locatorType, String locatorValue, String attributeName, String expectValue)
	{
		boolean result = false;
		String actualValue = "";
		try
		{
			if(locatorType.equalsIgnoreCase("xpath"))
			{
				actualValue = driver.findElement(By.xpath(locatorValue)).getAttribute(attributeName);
			}
			else if(locatorType.equalsIgnoreCase("id"))
			{
				actualValue = driver.findElement(By.xpath(locatorValue)).getAttribute(attributeName);
			}
			else if(locatorType.equalsIgnoreCase("name"))
			{
				actualValue = driver.findElement(By.xpath(locatorValue)).getAttribute(attributeName);
			}
			if(actualValue.equalsIgnoreCase(expectValue))
			{
				result = true;
			}
		}
		catch(Exception e)
		{
			
		}
		
		return result;
	}
	
	
	//validate element existing on webpage
	public static boolean validateElimentExists (WebDriver driver, String locatorType, String locatorValue)
	{
		boolean result = false;
		try
		{
			if(locatorType.equalsIgnoreCase("xpath"))
			{
				result = driver.findElement(By.xpath(locatorValue)).isDisplayed();
			}
			else if(locatorType.equalsIgnoreCase("id"))
			{
				result = driver.findElement(By.xpath(locatorValue)).isDisplayed();
			}
			else if(locatorType.equalsIgnoreCase("name"))
			{
				result = driver.findElement(By.xpath(locatorValue)).isDisplayed();
			}
		}
		catch(Exception e)
		{
			
		}
		
		return false;
	}
	
}
