package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	public WebDriver driver;

	//define element
	By myAccount = By.xpath("//h2[contains(text(),'My accounts')]");
	By myTransaction = By.xpath("//h2[contains(text(),'My transactions')]");
	By mainExpress = By.xpath("//h2[contains(text(),'Main expenses')]");
	By quickPay = By.xpath("//h2[contains(text(),'Quick pay')]");
	By sendMoney = By.xpath("//*[@id='menu_send']");
	By moveMoney = By.xpath("//*[@id=\"menu_quicktransfer\"]");
	By pay = By.xpath("//*[@id='menu_pay']");
	By overview = By.xpath("//*[@id='menu_account']");
	By closeModal = By.xpath("//*[@id=\\\"modal-close-btn\\\"]");
	
	
	//constructor
	public HomePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	public WebElement clickMyAccount() 
	{
		return driver.findElement(myAccount);
	}
	
	public WebElement clickMytransaction() 
	{
		return driver.findElement(myTransaction);
	}
	
	public WebElement clickMainExpress() 
	{
		return driver.findElement(mainExpress);
	}
	
	public WebElement clickQuickPay() 
	{
		return driver.findElement(quickPay);
	}
	
	public WebElement clickSendMoney() 
	{
		return driver.findElement(sendMoney);
	}
	
	public WebElement clickMoveMoney() 
	{
		return driver.findElement(moveMoney);
	}
	
	public WebElement clickPay() 
	{
		return driver.findElement(pay);
	}
	
	public WebElement clickOverview() 
	{
		return driver.findElement(overview);
	}
	
	public WebElement clickCloseModal() 
	{
		return driver.findElement(closeModal);
	}

}
