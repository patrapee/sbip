package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class LoanDetails {

	public WebDriver driver;

	//define element
	By repaymentButton = By.xpath("//button[@class='bt_cta primary']");
	By oneOffOption = By.xpath("//input[@value='ONE_OFF']/parent::span");
	By payAllOption = By.xpath("//input[@value='PTO']/parent::span");
	By continueButton = By.xpath("//button[@type='submit']");
	By fromAccount = By.xpath("//div[@class='Select-control']");
	By oneOffAccountSelected = By.xpath("//div[@class='Select-menu']/div[1]");
	By ptoAccountSelected = By.xpath("//div[@class='Select-menu']/div[1]");
	By amount = By.xpath("//input[@type = 'text']");
	By accountSelectedContinue = By.xpath("//button[@id='modalFooterContinueButton']");
	By confirmButton = By.xpath("//button[text()='Confirm']");
	
	
	//constructor
	public LoanDetails(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	public WebElement LoanRepayment() 
	{
		return driver.findElement(repaymentButton);
	}
	
	public WebElement OneOffSelection() 
	{
		return driver.findElement(oneOffOption);
	}
	
	public WebElement PayAllSelection() 
	{
		return driver.findElement(payAllOption);
	}
	
	public WebElement Continue() 
	{
		return driver.findElement(continueButton);
	}
	
	public WebElement FromAccount() 
	{
		return driver.findElement(fromAccount);
	}
	
	public WebElement OneOffAccountSelected() 
	{
		return driver.findElement(oneOffAccountSelected);
	}
	
	public WebElement PtoAccountSelected() 
	{
		return driver.findElement(ptoAccountSelected);
	}
	
	public WebElement FillingAmount() 
	{
		return driver.findElement(amount);
	}
	
	public WebElement AccountSelectedCont() 
	{
		return driver.findElement(accountSelectedContinue);
	}
	
	public WebElement Confirmation() 
	{
		return driver.findElement(confirmButton);
	}

}
