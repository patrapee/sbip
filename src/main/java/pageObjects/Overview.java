package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Overview {

	public WebDriver driver;

	//define element
	//TA environment
	By bncLoan = By.xpath("//span[contains(text(),'20062654973')]");
	//By bncLoan = By.xpath("//span[contains(text(),'0000020310472071')]");
	
	
	//constructor
	public Overview(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	public WebElement VerifyBNCLoan() 
	{
		return driver.findElement(bncLoan);
	}

}