package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SendMoney {

	public WebDriver driver;

	//define element
	By addRecipient = By.xpath("//*[@id='btn_modal_send_add_recipient']");
	By recipientName = By.xpath("//*[@id='product-form-input-name']");
	By nbcRecipient = By.xpath("//*[@id='dialog']/div/div[3]/div/section/form/section/div/div/div/div/div/div[2]/label/div/div/div[1]/span[1]");
	By transitNumber = By.xpath("//*[@id='product-form-input-transit']");
	By accountNumber = By.xpath("//*[@id='product-form-input-accountNumber']");
	By expireDate = By.xpath("//*[@id='product-form-input-cardExpiryDate']");
	By confirm = By.xpath("//button[@class='bt_cta primary']");
	
	//constructor
	public SendMoney(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	public WebElement AddRecipient() 
	{
		return driver.findElement(addRecipient);
	}
	
	public WebElement RecipientName() 
	{
		return driver.findElement(recipientName);
	}
	
	public WebElement NbcRecipient() 
	{
		return driver.findElement(nbcRecipient);
	}
	
	public WebElement TransitNumber() 
	{
		return driver.findElement(transitNumber);
	}
	
	public WebElement AccountNumber() 
	{
		return driver.findElement(accountNumber);
	}
	
	public WebElement ExpireDate() 
	{
		return driver.findElement(expireDate);
	}
	
	public WebElement Confirm() 
	{
		return driver.findElement(confirm);
	}
	
}
