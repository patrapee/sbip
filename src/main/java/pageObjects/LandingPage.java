package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {

	public WebDriver driver;

	//define element
	By userName = By.xpath("//*[@id='identity']");
	By password = By.xpath("//*[@id='password']");
	By login = By.xpath("//*[@id='loginForm']/div/div/button[1]");
	
	
	//constructor
	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	public WebElement Login() 
	{
		return driver.findElement(login);
	}
	
	public WebElement UserName() 
	{
		return driver.findElement(userName);
	}
	
	public WebElement Password() 
	{
		return driver.findElement(password);
	}
	
}
