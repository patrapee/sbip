package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MoveMoney {
	public WebDriver driver;

	//define element
	
	By fromAccount = By.xpath("//*[@id=\"react-select-2--value\"]/div[1]");
	By chooseFromAccount = By.xpath("//*[@id=\"react-select-2--option-0\"]");
	By toAccount = By.xpath("//*[@id=\"react-select-3--value\"]/div[1]");
	By chooseToAccount = By.xpath("//*[@id=\"react-select-3--option-0\"]");
	By continueMoveMoneyButton = By.xpath("//*[@id=\"transfer-between-accounts__continue-button\"]");
	By amount = By.id("transactionFormAmount");
	By confirmMoveMoneyButton = By.id("modalFooterContinueButton");
	By resultMessage = By.xpath("//*[@id=\"dialog\"]/div/div[3]/div/section/div/div/section/header/div/div[2]/h2");
	
	
	public WebElement getFromAccount() {
		return driver.findElement(fromAccount);
	}
	
	public WebElement chooseFromAccount() {
		return driver.findElement(chooseFromAccount);
	}
	
	public WebElement getToAccount() {
		return driver.findElement(toAccount);
	}
	
	public WebElement chooseToAccount() {
		return driver.findElement(chooseToAccount);
	}
	
	public WebElement clickContinue() {
		return driver.findElement(continueMoveMoneyButton);
	}
	
	public WebElement inputAmount() {
		return driver.findElement(amount);
	}
	
	public WebElement clickConfirm() {
		return driver.findElement(confirmMoveMoneyButton);
	}
	
	public WebElement getResultMessage() {
		return driver.findElement(resultMessage);
	}

	//constructor
	public MoveMoney(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
}
