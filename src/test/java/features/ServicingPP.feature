Feature: Personal loan payment

Scenario Outline: Customer pays personal loan with OneOff successfully
Given Customer login to "https://app-ta.bnc.ca" with <username> and <password>
When Customer select loan payment from loan details page with OneOff option
Then Customer successfully pay loan with OneOff option

Examples:
|username						|	password	|
|sbip2.test04+1+TA@gmail.com	|	tests999	|


Scenario: Customer pays personal loan with Pay all successfully
Given Customer login to "https://app-ta.bnc.ca" with <username> and <password>
When Customer select loan payment from loan details page with Pay all option
Then Customer successfully pay loan with OneOff option