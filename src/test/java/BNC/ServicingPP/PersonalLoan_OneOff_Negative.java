package BNC.ServicingPP;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import BNC.Login.Login;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;
import pageObjects.LandingPage;
import pageObjects.LoanDetails;
import pageObjects.Overview;
import pageObjects.SendMoney;
import resources.AssertionCompare;
import resources.Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class PersonalLoan_OneOff_Negative extends Base {

	@Given("^$")
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver = initializeDriver();

	}
	
	@When("^$")
	@Test(dataProvider="getData")
	public void PersonalLoanOneOff_Negative(String username,String password,String text) throws IOException, InterruptedException
	{

		// creating object to that class and invoke methods of it
		driver.get(prop.getProperty("url"));
		
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys(username);
		landing.Password().sendKeys(password);
		landing.Login().click();
		
		Thread.sleep(20000);
		HomePage homePage = new HomePage(driver);
		homePage.clickOverview().click();
		
		Thread.sleep(5000);
		Overview overview = new Overview(driver);
		overview.VerifyBNCLoan().click();
		
		Thread.sleep(5000);
		LoanDetails loanDetails = new LoanDetails(driver);
		loanDetails.LoanRepayment().click();
		
		Thread.sleep(5000);
		loanDetails.OneOffSelection().click();
		//Thread.sleep(3000);
		//new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(loanDetails.OneOffContinue()));
		loanDetails.Continue().click();
		
		Thread.sleep(5000);
		loanDetails.AccountSelectedCont().click();
		
		boolean actualResult = AssertionCompare.ValidateText(driver, "From which account would you like to make this payment?");
		Assert.assertTrue(actualResult, "[Error - The error message is not as expected]");
	
	}

	@Then("^Customer successfully login$")
	@AfterTest
	public void teardown()
	{
		
		driver.close();
		driver=null;
		
	}
	
	@DataProvider
	public Object[][] getData()
	{
		// Row stands for how many different data types test should run
		//column stands for how many values per each test
		
		Object[][] data = new Object[1][3];
		//0th row
		data[0][0]="sbip2.test04+1+TA@gmail.com";
		data[0][1]="tests999";
		data[0][2]="Valid user";
		
		return data;
		
	}
	
}
