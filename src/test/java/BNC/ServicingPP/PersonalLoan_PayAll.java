package BNC.ServicingPP;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import BNC.Login.Login;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;
import pageObjects.LandingPage;
import pageObjects.LoanDetails;
import pageObjects.Overview;
import pageObjects.SendMoney;
import resources.AssertionCompare;
import resources.Base;

public class PersonalLoan_PayAll extends Base {

	@Given("^$")
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver = initializeDriver();

	}
	
	@When("^$")
	@Test(dataProvider="getData")
	public void PersonalLoanPayAll(String username,String password,String text) throws IOException, InterruptedException
	{

		// creating object to that class and invoke methods of it
		driver.get(prop.getProperty("url"));
		
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys(username);
		landing.Password().sendKeys(password);
		landing.Login().click();
		
		// xpath of my account text //*[@id="main"]/div/div[1]/div[2]/div/h2
		//To wait for element visible
		//WebDriverWait wait = new WebDriverWait(driver, 30);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("//*[@id='menu_send']")));
		Thread.sleep(20000);
		HomePage homePage = new HomePage(driver);
		homePage.clickOverview().click();
		
		Thread.sleep(5000);
		Overview overview = new Overview(driver);
		overview.VerifyBNCLoan().click();
		
		Thread.sleep(5000);
		LoanDetails loanDetails = new LoanDetails(driver);
		loanDetails.LoanRepayment().click();
		
		Thread.sleep(5000);
		loanDetails.PayAllSelection().click();
		//Thread.sleep(3000);
		//new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(loanDetails.OneOffContinue()));
		loanDetails.Continue().click();
		
		Thread.sleep(5000);
		loanDetails.FromAccount().click();
		
		//driver.findElement(By.cssSelector("div.Select-menu")).findElement(By.xpath(String.format(".//div[text()='%s']", "Progress Account in CDN$ 24-291-91 : $-30,065.16"))).click();
		
		Thread.sleep(5000);
		loanDetails.PtoAccountSelected().click();
		
		Thread.sleep(5000);
		loanDetails.AccountSelectedCont().click();
		
		Thread.sleep(5000);
		loanDetails.Confirmation().click();
		
		//Assertion
		Thread.sleep(5000);
		boolean actualResult = AssertionCompare.ValidateText(driver, "Payment processed");
		Assert.assertTrue(actualResult, "[Error - The error message is not as expected]");
	
		
	}

	@Then("^Customer successfully login$")
	@AfterTest
	public void teardown()
	{
		
		driver.close();
		driver=null;
		
	}
	
	@DataProvider
	public Object[][] getData()
	{
		// Row stands for how many different data types test should run
		//column stands for how many values per each test
		
		Object[][] data = new Object[1][3];
		//0th row
		data[0][0]="sbip2.test04+1+TA@gmail.com";
		data[0][1]="tests999";
		data[0][2]="Valid user";
		
		return data;
		
	}
	
}
