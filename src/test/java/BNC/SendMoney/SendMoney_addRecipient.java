package BNC.SendMoney;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import BNC.Login.Login;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;
import pageObjects.SendMoney;
import resources.AssertionCompare;
import resources.Base;

public class SendMoney_addRecipient extends Base {
	
	@Given("^$")
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver = initializeDriver();

	}
	
	@When("^$")
	@Test(dataProvider="getData")
	public void addRecipient(String username,String password,String text) throws IOException, InterruptedException
	{

		// creating object to that class and invoke methods of it
		driver.get(prop.getProperty("url"));
		
		Login login = new Login();
		login.initialize();
		login.loginBase(username, password, text);
		
		// xpath of my account text //*[@id="main"]/div/div[1]/div[2]/div/h2
		//To wait for element visible
		//WebDriverWait wait = new WebDriverWait(driver, 30);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("//*[@id='menu_send']")));
		Thread.sleep(15000);
		//wait.until(ExpectedConditions.elementToBeClickable(By.id("//*[@id='menu_pay']")));
		HomePage homePage = new HomePage(driver);
		homePage.clickSendMoney().click();
		
		Thread.sleep(3000);
		SendMoney sendMoney = new SendMoney(driver);
		sendMoney.AddRecipient().click();
		
		sendMoney.RecipientName().sendKeys("Patrapee");
		sendMoney.AddRecipient().click();
		sendMoney.TransitNumber().sendKeys("12345");
		sendMoney.AccountNumber().sendKeys("1234567");
		sendMoney.ExpireDate().sendKeys("0131");
		sendMoney.Confirm().click();
		Thread.sleep(5000);
		
		//String actualResult = AssertionCompare.validateTextExists(driver, "", "");
		//Assert.assertEquals(actualResult, "Success", "[Error - Case is fail]");
	
		
	}

	@Then("^Customer successfully login$")
	@AfterTest
	public void teardown()
	{
		
		driver.close();
		driver=null;
		
	}
	
	@DataProvider
	public Object[][] getData()
	{
		// Row stands for how many different data types test should run
		//column stands for how many values per each test
		
		Object[][] data = new Object[1][3];
		//0th row
		data[0][0]="sbip.test02+ta@gmail.com";
		data[0][1]="tests999";
		data[0][2]="Valid user";
		
		return data;
		
	}
	

}
