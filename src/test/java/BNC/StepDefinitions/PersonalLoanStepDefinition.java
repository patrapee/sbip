package BNC.StepDefinitions;

import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;
import pageObjects.LandingPage;
import pageObjects.LoanDetails;
import pageObjects.Overview;
import resources.AssertionCompare;
import resources.Base;

public class PersonalLoanStepDefinition extends Base {

	@Given("^Customer login to \"([^\"]*)\" with (.+) and (.+)$")
    public void customer_login_to_something_with_something(String strArg1, String strArg2, String strArg3) throws Throwable {
        
		//Open browser and login
		driver = initializeDriver();
		
		driver.get(strArg1);
		
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys(strArg2);
		landing.Password().sendKeys(strArg3);
		landing.Login().click();
		
    }

    @When("^Customer select loan payment from loan details page with OneOff option$")
    public void customer_select_loan_payment_from_loan_details_page() throws Throwable {
        
    	//Loan payment customer journey
    	Thread.sleep(20000);
		HomePage homePage = new HomePage(driver);
		homePage.clickOverview().click();
		
		Thread.sleep(5000);
		Overview overview = new Overview(driver);
		overview.VerifyBNCLoan().click();
		
		Thread.sleep(5000);
		LoanDetails loanDetails = new LoanDetails(driver);
		loanDetails.LoanRepayment().click();
		
		Thread.sleep(5000);
		loanDetails.OneOffSelection().click();
		//Thread.sleep(3000);
		//new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(loanDetails.OneOffContinue()));
		loanDetails.Continue().click();
		
		Thread.sleep(5000);
		loanDetails.FromAccount().click();
		
		//driver.findElement(By.cssSelector("div.Select-menu")).findElement(By.xpath(String.format(".//div[text()='%s']", "Progress Account in CDN$ 24-291-91 : $-30,065.16"))).click();
		
		Thread.sleep(5000);
		loanDetails.OneOffAccountSelected().click();
		loanDetails.FillingAmount().sendKeys("1");
		
		Thread.sleep(5000);
		loanDetails.AccountSelectedCont().click();
		
		Thread.sleep(5000);
		loanDetails.Confirmation().click();
    	
    }

    @Then("^Customer successfully pay loan with OneOff option$")
    public void customer_successfully_pay_loan_with_oneoff_option() throws Throwable {
        
    	//validating successfully payment
    	boolean actualResult = AssertionCompare.ValidateText(driver, "Payment processed");
		Assert.assertTrue(actualResult, "[Error - The message is not as expected]");
		
		//close browser
		driver.close();
		driver=null;
    	
    }
	
}
