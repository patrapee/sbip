package BNC;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.HomePage;
import pageObjects.LandingPage;
import resources.Base;

public class ValidateHomepage extends Base {
	
	public static Logger log = LogManager.getLogger(Base.class.getName());

	@BeforeTest
	public void initialize() throws IOException
	{

		driver =initializeDriver();
		log.info("Driver is initialized successfully");

		driver.get(prop.getProperty("url"));
		log.info("Navigated to Home page");
		
	}

	@Test
	public void validateAppTitle() throws IOException
	{

		// creating object to that class and invoke methods of it
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys("portal.vsd02+01@gmail.com");
		landing.Password().sendKeys("Buzzer999");
		landing.Login().click();
		
		HomePage homePage = new HomePage(driver);
		
		//compare the text from the browser with actual text.- Error..
		Assert.assertEquals(homePage.clickMyAccount().getText(), "My accounts");
		System.out.println("validate my account");
		
		Assert.assertEquals(homePage.clickMytransaction().getText(), "My transactions");
		System.out.println("validate my transactions");
		
		Assert.assertEquals(homePage.clickMainExpress().getText(), "Main expenses");
		System.out.println("validate main expenses");
		
		Assert.assertEquals(homePage.clickQuickPay().getText(), "Quick pay");
		System.out.println("validate quick pay");
		
		log.info("Successfully validated Text message");

	}
	@AfterTest
	public void teardown()
	{

		driver.close();
		driver=null;

	}
	
}
