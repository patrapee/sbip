package BNC.Login;

import java.io.IOException;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.LandingPage;
import resources.Base;

public class Login extends Base{

	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver = initializeDriver();  

	}
	
	@Given("^Customer is trying to login to SBIP$")
	@When("^Customer input the correct password$")
	@Then("^Customer successfully login$")
	@Test(dataProvider="getData")
	public void loginBase(String username,String password,String text) throws IOException, InterruptedException
	{

		//one is inheritance

		// creating object to that class and invoke methods of it
		driver.get(prop.getProperty("url"));
		
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys(username);
		//Thread.sleep(5000);
		landing.Password().sendKeys(password);
		//Thread.sleep(5000);
		landing.Login().click();
		
		}

	@AfterTest
	public void teardown()
	{
		
		driver.close();
		driver=null;
		
	}
	
	@DataProvider
	public Object[][] getData()
	{
		// Row stands for how many different data types test should run
		//column stands for how many values per each test
		
		Object[][] data = new Object[1][3];
		//0th row
		data[0][0]="sbip.test02+ta@gmail.com";
		data[0][1]="tests999";
		data[0][2]="Valid user";
		
		return data;
		
	}
	
}
