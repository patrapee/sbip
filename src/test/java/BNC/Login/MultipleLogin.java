package BNC.Login;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.LandingPage;
import resources.Base;

public class MultipleLogin extends Base {
	
	public static Logger log = LogManager.getLogger(Base.class.getName());
	
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver = initializeDriver();

	}
	
	@Given("^Customer is trying to login to SBIP$")
	@When("^Customer input the correct password$")
	@Then("^Customer successfully login$")
	@Test(dataProvider="getData")
	public void mulLogin(String Username,String Password,String text) throws IOException
	{

		//one is inheritance

		// creating object to that class and invoke methods of it
		driver.get(prop.getProperty("url"));
		
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys(Username);
		landing.Password().sendKeys(Password);
		landing.Login().click(); //driver.findElement(By.css()

		log.info(text);
		
		}

	@AfterTest
	public void teardown()
	{
		
		driver.close();
		driver=null;
		
	}

	
	@DataProvider
	public Object[][] getData()
	{
		// Row stands for how many different data types test should run
		//column stands for how many values per each test
		
		// Array size is 2
		// 0,1
		Object[][] data = new Object[2][3];
		//0th row
		data[0][0]="portal.vsd02+01@gmail.com";
		data[0][1]="Buzzer999";
		data[0][2]="Valid user";
		//1st row
		data[1][0]="portal.vsd02+01+gg@gmail.com";
		data[1][1]="Buzzer888";
		data[1][2]="Invalid user";
		
		return data;
		
	}
	
}
