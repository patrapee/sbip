package BNC.MoveMoney;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;
import pageObjects.LandingPage;
import pageObjects.MoveMoney;
import resources.AssertionCompare;
import resources.Base;

public class Move_Money extends Base {
	@Given("^$")
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver = initializeDriver();

	}
	
	@When("^$")
	
	@Test(dataProvider="getData")
	public void moveMoney(String username, String password, String amount) throws InterruptedException
	{
		driver.get(prop.getProperty("url"));
		
		LandingPage landing = new LandingPage(driver);
		landing.UserName().sendKeys(username);
		landing.Password().sendKeys(password);
		landing.Login().click();
		
		Thread.sleep(10000);
		HomePage homepage = new HomePage(driver);
		homepage.clickMoveMoney().click();
		
		Thread.sleep(3000);
		MoveMoney moveMoney = new MoveMoney(driver);
		moveMoney.getFromAccount().click();
		moveMoney.chooseFromAccount().click();
		moveMoney.getToAccount().click();
		moveMoney.chooseToAccount().click();
		moveMoney.clickContinue().click();
		
		Thread.sleep(3000);
		moveMoney.inputAmount().sendKeys(amount);
		moveMoney.clickConfirm().click();
		
		//Assertion
		Thread.sleep(3000);
		boolean actualResult = AssertionCompare.ValidateText(driver, "Transfer successful"); //check if screen contain text "Transfer Successful".
		Assert.assertTrue(actualResult, "[Error - The error message is not as expected]"); //Do nothing if 1st param is True, else 2nd param will be printed.
		
	}

	@Then("^$")
	@AfterTest
	public void teardown()
	{
		System.out.println("tea");
		driver.quit();
		driver=null;
		System.out.println("xxx");
	}
	@DataProvider
	public Object[][] getData()
	{
		// Row stands for how many different data types test should run
		//column stands for how many values per each test
		
		Object[][] data = new Object[1][3];
		//0th row
		data[0][0]="sbip2.test04+1+TA@gmail.com";
		//data[0][0]="sbip2.test01+PP+TI@gmail.com";
		data[0][1]="tests999";
		data[0][2]="0.1";
		
		return data;
		
	}
	
}
