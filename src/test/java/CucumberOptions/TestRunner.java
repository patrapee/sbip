package CucumberOptions;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)
@CucumberOptions(
	features = "src/test/java/features/ServicingPP.feature",
	glue = "BNC.StepDefinitions")

public class TestRunner extends AbstractTestNGCucumberTests {

}
